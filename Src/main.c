/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile uint32_t l_pulse_count,l_positions,r_positions,r_pulse_count;
volatile float r_speed,l_speed;


//Definicje Ledow
#define LED_1_ON HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,1);
#define LED_1_OFF HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,0);
#define LED_2_ON HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,1);
#define LED_2_OFF HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,0);
#define LED_3_ON HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,1);
#define LED_3_OFF HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,0);
#define LED_4_ON HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,1);
#define LED_4_OFF HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,0);
#define LED_5_ON HAL_GPIO_WritePin(LED5_GPIO_Port,LED5_Pin,1);
#define LED_5_OFF HAL_GPIO_WritePin(LED5_GPIO_Port,LED5_Pin,0);
#define LED_6_ON HAL_GPIO_WritePin(LED6_GPIO_Port,LED6_Pin,1);
#define LED_6_OFF HAL_GPIO_WritePin(LED6_GPIO_Port,LED6_Pin,0);


//Definicje swichy
#define S1 !HAL_GPIO_ReadPin(SW1_GPIO_Port,SW1_Pin)
#define S2 !HAL_GPIO_ReadPin(SW2_GPIO_Port,SW2_Pin)
#define S3 !HAL_GPIO_ReadPin(SW3_GPIO_Port,SW3_Pin)

#define start HAL_GPIO_ReadPin(START_GPIO_Port,START_Pin)



//#define start (PINC &_BV(2))

/* definicja portow dla sharpow */
#define SHARP_CL	HAL_GPIO_ReadPin(SHARP4_GPIO_Port,SHARP4_Pin)
#define SHARP_CP	HAL_GPIO_ReadPin(SHARP5_GPIO_Port,SHARP5_Pin)
#define SHARP_P		HAL_GPIO_ReadPin(SHARP3_GPIO_Port,SHARP3_Pin)
#define SHARP_L		HAL_GPIO_ReadPin(SHARP6_GPIO_Port,SHARP6_Pin)
#define SHARP_SP	HAL_GPIO_ReadPin(SHARP2_GPIO_Port,SHARP2_Pin)
#define SHARP_SL	HAL_GPIO_ReadPin(SHARP1_GPIO_Port,SHARP1_Pin)



int sharp[6] = {0,0,0,0,0,0};
volatile uint16_t pomiary[12];
int max = 1;
int prawiemax = 100;
int obrot = 500;
int luk=300;
int zwrot = 200;
int wrog =1;
int poczatek=0;
int czas =0;



void jazdawstecz(int a, int b)
{
	  TIM1->CCR2=a;
	  TIM1->CCR3=b;
	  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,1);
	  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,0);
	  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,0);
	  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,1);
}

void jazdawprzod(int a, int b)
{
	  TIM1->CCR2=a;
	  TIM1->CCR3=b;
	  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,0);
	  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,1);
	  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,1);
	  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,0);
}

void obrotwlewo(int a, int b)
{
	  TIM1->CCR2=a;
	  TIM1->CCR3=b;
	  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,0);
	  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,1);
	  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,0);
	  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,1);
}

void obrotwprawo(int a, int b)
{
	  TIM1->CCR2=a;
	  TIM1->CCR3=b;
	  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,1);
	  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,0);
	  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,1);
	  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,0);
}

void zatrzymanie(void)
{
	  TIM1->CCR2=999;
	  TIM1->CCR3=999;
	  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,1);
	  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,1);
	  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,1);
	  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,1);

};

void sharpy()
{

	if (!SHARP_L)
		sharp[0] = 1;
	else
		sharp[0] = 0;

	if (!SHARP_CP)
		sharp[3] = 1;
	else
		sharp[3] = 0;

	if (!SHARP_CL)
		sharp[2] = 1;
	else
		sharp[2] = 0;

	if (!SHARP_P)
		sharp[5] = 1;
	else
	    sharp[5] = 0;

	if (!SHARP_SP)
		sharp[4] = 1;
	else
	    sharp[4] = 0;

	if (!SHARP_SL)
		sharp[1] = 1;
	else
	    sharp[1] = 0;
}
void ledsharp()
{
		  if (sharp[0])
		  LED_1_ON
		  else
		  LED_1_OFF
		  sharpy();
		  if (sharp[1])
		  LED_5_ON
		  else
		  LED_5_OFF
		  sharpy();
		  if (sharp[2])
		  LED_2_ON
		  else
		  LED_2_OFF
		  sharpy();
		  if (sharp[3])
		  LED_3_ON
		  else
		  LED_3_OFF
		  sharpy();
		  if (sharp[4])
		  LED_6_ON
		  else
		  LED_6_OFF;
		  sharpy();
		  if (sharp[5])
		  LED_4_ON
		  else
		  LED_4_OFF
}

void odliczanie()
{
LED_1_ON
LED_2_ON
LED_3_ON
LED_4_ON
LED_5_ON
LED_6_ON
HAL_Delay(1000);
LED_5_OFF
LED_6_OFF
HAL_Delay(1000);
LED_4_OFF
HAL_Delay(1000);
LED_3_OFF
HAL_Delay(1000);
LED_2_OFF
HAL_Delay(1000);
LED_1_OFF
}

void walka()
{
	if ((sharp[2]) && (sharp[3]) || ((sharp[1]) && (sharp[4]) ))
	{
	jazdawprzod(max,max);
	HAL_Delay(5);
	}
	else
	if (sharp[2])
	{
	jazdawprzod((0.9 * max),max);
	wrog=-1;
	HAL_Delay(5);
	}
	else
	if (sharp[3])
	{
	jazdawprzod(max,(0.9 * max));
	wrog=1;
	HAL_Delay(5);
	}
	else
	if (sharp[1])
	{
		jazdawprzod(350,700);
	wrog=2;
	}
	else
	if (sharp[4])
	{
	jazdawprzod(700,350);
	wrog=-2;
	}
	else
	if (sharp[5])
	{
	obrotwprawo(zwrot,zwrot);
	wrog=3;
	}
	else
	if (sharp[0])
	{
	obrotwlewo(zwrot,zwrot);
	wrog=-3;
	}
	else
	if (wrog==(-1))
	{
	obrotwlewo(obrot,obrot);
	}
	else
	if (wrog==1)
	{
	obrotwprawo(obrot,obrot);
	}
	else
	if (wrog==2)
	{
	obrotwprawo(obrot,obrot);
	}
	else
	if (wrog==(-2))
	{
	obrotwlewo(obrot,obrot);
	}
	else
	if (wrog==3)
	{
	obrotwprawo(zwrot,zwrot);
	}
	else
	if (wrog==-3)
	{
	obrotwlewo(zwrot,zwrot);
	}
	HAL_Delay(2);
	zatrzymanie();
}


volatile int opoznienie=1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
 if(htim->Instance == TIM10)
 {
/*
	 l_speed=(((TIM3->CNT)/38912.0f)*100.0f);
	 TIM3->CNT=0;
	 r_speed=(((TIM2->CNT)/38912.0f)*100.0f);
	 TIM2->CNT=0;
*/
 }
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
float napiecie;
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_I2C3_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM10_Init();

  /* USER CODE BEGIN 2 */
  HAL_ADC_Start_DMA(&hadc1, pomiary, 12);
  HAL_GPIO_WritePin(FBLE_GPIO_Port,FBLE_Pin,1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
  TIM1->CCR2=999;
  TIM1->CCR3=999;
  HAL_GPIO_WritePin(MOT2B_GPIO_Port,MOT2B_Pin,0);
  HAL_GPIO_WritePin(MOT2A_GPIO_Port,MOT2A_Pin,1);
  HAL_GPIO_WritePin(MOT3B_GPIO_Port,MOT3B_Pin,1);
  HAL_GPIO_WritePin(MOT3A_GPIO_Port,MOT3A_Pin,0);

  HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
  HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
  HAL_TIM_Base_Start_IT(&htim10);
HAL_Delay(100);
LED_1_ON;
LED_2_ON;
LED_3_ON;
LED_4_ON;
LED_5_ON;
LED_6_ON;
HAL_Delay(500);
LED_1_OFF;
LED_2_OFF;
LED_3_OFF;
LED_4_OFF;
LED_5_OFF;
LED_6_OFF;
zatrzymanie();



  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
	  napiecie=((pomiary[3]/4096.0)*25.868);
	  sharpy();
	  ledsharp();
	  HAL_Delay(1);
	  //Switch start begin
	  if(S3)
	  {
		  if(opoznienie)
		  {
			  	odliczanie();
			  	opoznienie=0;
		  }
	  while(1)
	  {
		  {
		  sharpy();
		  ledsharp();
		  if (poczatek)
		  {

		  	while(1)
		  	{
		  		if (!SHARP_CL || !SHARP_CP || !SHARP_L || !SHARP_P || !SHARP_SL || !SHARP_SP)
		  		break;
		  		jazdawprzod(400,400);
		  		HAL_Delay(5);
		  		czas=czas+5;
		  		if (czas > 100)
		  		{
		  			zatrzymanie();
		  			break;
		  		}

		  	}
		  }

		  poczatek=0;

		  /*if ((LLINIA) | (PLINIA))
		  {
		  jazdawstecz(max,max);
		  HAL_Delay(100);
		  }
		  */

		  walka();
	  }
	  }
	  }
	  //Switch start end


	  //Module start begin
while(start)
{
sharpy();
ledsharp();
if (poczatek)
{
	//odliczanie();
	while(1)
	{
		if (!SHARP_CL || !SHARP_CP || !SHARP_L || !SHARP_P || !SHARP_SL || !SHARP_SP)
		break;
		jazdawprzod(400,400);
		HAL_Delay(5);
		czas=czas+5;
		if (czas > 100)
		{
			zatrzymanie();
			break;
		}

	}
}

poczatek=0;

/*if ((LLINIA) | (PLINIA))
{
jazdawstecz(max,max);
HAL_Delay(100);
}
*/

walka();

}
	  //Module start end

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 84;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
